// node test.js c857d9dc88cca6ac08d3017df793c9f4 b7069b4a62250d46c3566a8e4225fb0a

const FITBIT_KEY = "c857d9dc88cca6ac08d3017df793c9f4";
const FITBIT_SECRET = "b7069b4a62250d46c3566a8e4225fb0a";
const FITBIT_CALLBACK = 'http://fit-bit-pong.herokuapp.com/login';
// const FITBIT_CALLBACK = 'http://localhost:3000/login';

var nunjucks = require('nunjucks')
var gameloop = require('node-gameloop');

var express = require('express')
var cookieParser = require('cookie-parser')

var app = express();

//
app.use(cookieParser('sess'));
app.use(express.static(__dirname + '/public'))

nunjucks.configure('', {
    autoescape: true,
    express: app
});

var http = require('http').Server(app);
var io = require('socket.io')(http, {
  // 'transports': 'xhr-polling', // THIS WAS CAUSING CLIENTS TO STAY ONLINE EVEN WHEN DISCONNECTING
  // "polling duration": 100,
  // 'pingInterval': 1000
});

// start the loop at 30 fps (1000/30ms per frame) and grab its id
var id = gameloop.setGameLoop(function(delta) {
  io.emit('update', delta);
  // `delta` is the delta time from the last frame
  // console.log('Hi there! (frame=%s, delta=%s)', frameCount++, delta);
}, 1000 / 60);





var users = [];
app.get('/', function(req, res){
  console.log("logged: "+newLogin);
  res.render('index.html', { users: users, logged: newLogin});
});

var fitbitClient = require( "./lib/fitbit_client.js" )
  (FITBIT_KEY, FITBIT_SECRET, FITBIT_CALLBACK);

var token;
var newLogin = false;
app.get('/login', function (req, res) {
  fitbitClient.getAccessToken(req, res, function (error, newToken) {
    if(newToken) {
      token = newToken;
      newLogin = true;
      res.redirect('/');
    }
  });
});

app.get('/getStuff', function (req, res) {
  fitbitClient.apiCall('GET', '/user/-/profile.json',
    {token: {oauth_token_secret: token.oauth_token_secret,
             oauth_token: token.oauth_token}},
    function(err, resp, json) {
      if (err) return res.send(err, 500);
      res.json(json);
  });
});

app.get('/cookie', function(req, res) {
  res.send('wahoo!');
});

//Listen to the connection
var clientsId = [];
io.on('connection', function(socket){
  var bNewClient = true;
  for (id in clientsId){
    if (id === socket.client.id){
      bNewClient = false;
    }
  }

  if (bNewClient){
    console.log("New client: "+socket.client.id);
    clientsId.push(socket.client.id);

    if (newLogin){
      newLogin = false;

      // fitbitClient.apiCall('GET', '/user/-/activities/date/today.json',
      fitbitClient.apiCall('GET', '/user/-/profile.json',
        {token: {oauth_token_secret: token.oauth_token_secret,
                 oauth_token: token.oauth_token}},
        function(err, resp, json) {
          if (err) return res.send(err, 500);
          var userFound = false;
          for (var i = 0; i < users.length; i++) {
              if (users[i].oauth_token_secret == token.oauth_token_secret && users[i].oauth_token == token.oauth_token) {
                userFound = true;
              }
          }
          if (!userFound){
            console.log("New user");
            var speed = json["user"]["averageDailySteps"]/10000;
            if (speed == 0 ) speed = 0.01;

            users.push({
              name:  json["user"]["displayName"],
              oauth_token_secret: token.oauth_token_secret,
              oauth_token: token.oauth_token,
              client_id: socket.client.id,
              speed: speed
            });
          } else {
            console.log("Found user - update client id");
            for (var i=0; i<users.length; i++){
              if (users[i].oauth_token_secret == token.oauth_token_secret && users[i].oauth_token == token.oauth_token) {
                users[i].client_id = socket.client.id;
              }
            }
          }

          console.log(users);
          console.log("New user finito");


          if (users[users.length-2]){
            io.emit('player00Name',
              {name:users[users.length-2].name, speed:users[users.length-2].speed});
          }
          if (users[users.length-1]) io.emit('player01Name',
            {name:users[users.length-1].name, speed:users[users.length-1].speed})

          //Emit data related to speed
          io.emit('restartBall', '');
      });
    }
  }

  console.log("Updated client list: ");
  console.log(clientsId);

  socket.on('disconnect', function () {
    console.log("Client disconnected: "+socket.client.id);
    io.emit('disconnected');

    //Delete user from list
    var index = clientsId.indexOf(socket.client.id);
    if (index > -1) {
        clientsId.splice(index, 1);
    }
    for (var i = 0; i < users.length; i++) {
        if (users[i].client_id == socket.client.id) {
          users.splice(i, 1);
        }
    }


    console.log("Updated client list: ");
    console.log(clientsId);
    console.log(users);




  });

  //On a specific message
  socket.on('pos', function(msg){
    if (users[users.length-2] && users[users.length-1]){
      if (socket.client.id == users[users.length-2].client_id){
        // console.log("player00 -  " + msg);
        userData = {
          pos: msg,
          speed: users[users.length-2].speed
          // speed: 1.289 //Fake speed
        }
        io.emit('player00', userData);
      }
      else if (socket.client.id == users[users.length-1].client_id){
        userData = {
          pos: msg,
          speed: users[users.length-1].speed
        }
        // console.log("player01 - " + msg);
        io.emit('player01', userData);
      }
    }
  });
});

http.listen(process.env.PORT || 3000, function(){
  console.log('listening on *:3000');
});
