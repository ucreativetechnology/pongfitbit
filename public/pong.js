//Variable speed
var player00Speed = 4
var player01Speed = 4

//socket
var socket = io();

//Get message from Server
var player00Pos = 0;
var player01Pos = 0;
var player00Score = 0;
var player01Score = 0;

var frameCount = 0;
socket.on('update', function(delta){
  // console.log('Hi there! (frame=%s, delta=%s)', frameCount++, delta);
  update();
  render();
  // animate(step);
});

var player00Name = ""
var player01Name = ""
socket.on('player00Name', function(msg){
  console.log(msg);
  player00Name = msg.name;
  player00Speed = 10*msg.speed
});
socket.on('player01Name', function(msg){
  player01Name = msg.name;
  player01Speed = 10*msg.speed
});

socket.on('player00', function(msg){
  player00Pos = msg.pos;
  player00Speed = 10*msg.speed
});

socket.on('player01', function(msg){
  player01Pos = msg.pos;
  player01Speed = 10*msg.speed
});

socket.on('restartBall', function(msg){
  console.log("restart ball");
  resetGame();
});
socket.on('disconnected', function(msg){
  player00Score = 0;
  player01Score = 0;
});


//
// var animate = window.requestAnimationFrame ||
//   window.webkitRequestAnimationFrame ||
//   window.mozRequestAnimationFrame ||
//   function(callback) { window.setTimeout(callback, 1000/60) };
// var animate = function(callback) { window.setTimeout(callback, 1000/60) };


var canvas = document.createElement('canvas');
var width = 600;
var height = 600;
canvas.width = width;
canvas.height = height;
var context = canvas.getContext('2d');

//GUI
var SpeedControls = function() {
  this.player00Speed= 4;
  this.player01Speed = 4;
};


window.onload = function() {
  document.body.appendChild(canvas);
  // animate(step); //animate var with step function as a calllback

  // var text = new SpeedControls();
  // var gui = new dat.GUI();
  // gui.add(text, 'player00Speed', 1, 15).onChange(function(newValue) {
  //   player00Speed = newValue;
  // });
  // gui.add(text, 'player01Speed', 1, 15).onChange(function(newValue) {
  //   player01Speed = newValue;
  // });
};

// var step = function() {
//   update();
//   render();
//   animate(step);
// };

var paddle00 = new Paddle(width*0.5 - 25, 580, 80, 16);
var paddle01 = new Paddle(width*0.5 - 25, 10, 80, 16);
var player00 = new Player(paddle00);
var player01 = new Player(paddle01);
var computer = new Computer();
var ball = new Ball(width*0.5, 300);

function resetGame(){
  paddle00.setPos(width*0.5 - 25, 580);
  paddle01.setPos(width*0.5 - 25, 10);
  // player00.update(175, player00Speed);
  // player01.update(175, player01Speed);
  ball.reset();
}


var update = function() {
    ball.update(player00.paddle, player01.paddle);
    player00.update(player00Pos, player00Speed);
    player01.update(player01Pos, player01Speed);
    player00Pos = 0;
    player01Pos = 0;

    routePongControls();
};

var render = function() {
  context.fillStyle = "#fff275";
  context.fillRect(0, 0, width, height);

  context.fillStyle = "#3b5166";
  context.font = "48px serif";
  if (player01Name != ""){
    context.fillText(player01Name, 10, 50);
    context.fillText("Avg. daily steps: "+parseInt(player01Speed*1000), 10, 50+50);
    var scoreStr = "";
    for (var i=0;i<player01Score;i++) scoreStr+="•";
    context.fillText(scoreStr, 10, 50+50+50);
  }
  if (player00Name != ""){
    context.fillText(player00Name, 10, height - 100);
    context.fillText("Avg. daily steps: "+parseInt(player00Speed*1000), 10, height - 100 + 50);
    var scoreStr = "";
    for (var i=0;i<player00Score;i++) scoreStr+="•";
    context.fillText(scoreStr, 10, height - 100 + 50+50);
  }

  var date = getDate();
  context.fillText(date, 10, height*0.5);

  player00.render();
  player01.render();
  // // computer.render();
  ball.render();
};


function routePongControls(){
  for(var key in keysDown) {
    var value = Number(key);
    if(value == 37) { // left arrow
      socket.emit('pos', -1);
    } else if (value == 39) { // right arrow
      socket.emit('pos', 1);
    } else {
      // socket.emit('pos', 0);
    }
  }
}

function Paddle(x, y, width, height) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.x_speed = 0;
  this.y_speed = 0;
}

Paddle.prototype.setPos = function(x, y) {
  this.x = x;
  this.y = y;
}

Paddle.prototype.render = function() {
  context.fillStyle = "#ff3c38";
  context.fillRect(this.x, this.y, this.width, this.height);
};


//Player & computer classes
function Player(paddle) {
  console.log(paddle.x);
  this.paddle = paddle;//Set size and pos for paddle
}

Player.prototype.render = function() {
  this.paddle.render();
};

Player.prototype.update = function(pos, speed) {

  this.paddle.move(pos*speed, 0);

  // for(var key in keysDown) {
  //   var value = Number(key);
  //   if(value == 37) { // left arrow
  //     this.paddle.move(-player1Speed, 0);
  //   } else if (value == 39) { // right arrow
  //     this.paddle.move(player1Speed, 0);
  //   } else {
  //     this.paddle.move(0, 0);
  //   }
  // }
};

Paddle.prototype.move = function(x, y) {
  this.x += x;
  this.y += y;
  this.x_speed = x;
  this.y_speed = y;
  if(this.x < 0) { // all the way to the left
    this.x = 0;
    this.x_speed = 0;
  } else if (this.x + this.width > width) { // all the way to the right
    this.x = width - this.width;
    this.x_speed = 0;
  }
}


function Computer() {
  this.paddle = new Paddle(175, 10, 80, 16);//Set size and pos for paddle
}

Computer.prototype.render = function() {
  this.paddle.render();
};

//Ball object

function Ball(x, y) {
  this.x = x;
  this.y = y;
  this.x_speed = 0;
  this.y_speed = 8;
  this.radius = 8;
}

Ball.prototype.render = function() {
  context.beginPath();
  context.arc(this.x, this.y, this.radius, 2 * Math.PI, false);
  context.fillStyle = "#a23e48";
  context.fill();
};

Ball.prototype.reset = function(){
  this.x_speed = 0;
  this.y_speed = 8;
  this.x = width*0.5;
  this.y = height*0.5;
}

Ball.prototype.update = function(paddle1, paddle2) {
  this.x += this.x_speed;
  this.y += this.y_speed;
  var top_x = this.x - 5;
  var top_y = this.y - 5;
  var bottom_x = this.x + 5;
  var bottom_y = this.y + 5;

  if(this.x - 5 < 0) { // hitting the left wall
    this.x = 5;
    this.x_speed = -this.x_speed;
  } else if(this.x + 5 > width) { // hitting the right wall
    this.x = width-5;
    this.x_speed = -this.x_speed;
  }

  if(this.y < 0 || this.y > height) { // a point was scored
    if (this.y < 0 ) player00Score++;
    if (this.y > height ) player01Score++;
    this.x_speed = 0;
    this.y_speed = 8;
    this.x = width*0.5;
    this.y = height*0.5;

  }

  if(top_y > height*0.5) {
    if(top_y < (paddle1.y + paddle1.height) && bottom_y > paddle1.y && top_x < (paddle1.x + paddle1.width) && bottom_x > paddle1.x) {
      // hit the player's paddle
      this.y_speed = -8;
      this.x_speed += (paddle1.x_speed / 2);
      this.y += this.y_speed;
    }
  } else {
    if(top_y < (paddle2.y + paddle2.height) && bottom_y > paddle2.y && top_x < (paddle2.x + paddle2.width) && bottom_x > paddle2.x) {
      // hit the computer's paddle
      this.y_speed = 8;
      this.x_speed += (paddle2.x_speed / 2);
      this.y += this.y_speed;
    }
  }
};


var keysDown = {};

window.addEventListener("keydown", function(event) {
  keysDown[event.keyCode] = true;
});

window.addEventListener("keyup", function(event) {
  delete keysDown[event.keyCode];
});

function getDate(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  if(dd<10) {
      dd='0'+dd
  }

  if(mm<10) {
      mm='0'+mm
  }

  today = dd+'/'+mm+'/'+yyyy;
  return today;
}
